declare interface MediaInfo {
    id: string;
    metadata: MediaMetadata;
    type: 'MediaInfo';
};

declare interface MediaMetadata {
    extractor?: string;
    uploader_id?: string;
    uploader?: string;
    title?: string;
    duration_string?: string;
    width?: number;
    height?: number;
    acodec?: string;
    vcodec?: string;
    channel?: string;
    fps?: number;
    ext?: string;
    id?: string;
    type: 'MediaMetadata';
};

declare interface ServerEvent {
    job: Job;
    info?: Info;
    error?: string;
    result_url?: string;
    type: 'ServerEvent';
}

declare interface Info {
    extractor?: string;
    uploader_id?: string;
    uploader?: string;
    uploader_url?: string;
    title?: string;
    duration_string?: string;
    width?: number;
    height?: number;
    acodec?: string;
    vcodec?: string;
    channel?: string;
    channel_url?: string;
    fps?: number;
    ext?: string;
    id?: string;
    upload_date?: string;
    description?: string;
    thumbnails?: Thumbnail[];
    type: 'Info';
}

declare interface Thumbnail {
    url: string;
    preference: number;
    id: string;
    height: number;
    width: number;
    resolution: string;
    type: 'Thumbnail';
}

declare interface Job {
    ID: number;
    Url: string;
    Added: string;
    Status: number;
    type: 'Job';
}

enum QueueItemState {
    Queued = 0,
    Running,
    Completed,
    Failed,
}

export type { ServerEvent, Info, Thumbnail, Job, MediaInfo, MediaMetadata }
export { QueueItemState }
