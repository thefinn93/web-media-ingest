import { reactive } from 'vue'
import type { ServerEvent } from './structs'

let queue: Record<number, ServerEvent> = {}
export const store = reactive({
    version: '?',
    queue: queue,
})
