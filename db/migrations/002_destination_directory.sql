-- +goose Up
-- +goose StatementBegin
ALTER TABLE jobs ADD COLUMN destination TEXT NOT NULL DEFAULT "";
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE jobs DROP COLUMN destination;
-- +goose StatementEnd
