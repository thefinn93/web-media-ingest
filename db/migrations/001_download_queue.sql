-- +goose Up
-- +goose StatementBegin
CREATE TABLE jobs (
    id INTEGER PRIMARY KEY,
    url TEXT NOT NULL,
    last_change TEXT NOT NULL,
    status INTEGER NOT NULL
);

CREATE TABLE infojson (
    url TEXT NOT NULL,
    data BLOB NOT NULL, -- JSON-encoded infojson from yt-dlp
    last_used TEXT NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE jobs;
DROP TAbLE metadata;
-- +goose StatementEnd
