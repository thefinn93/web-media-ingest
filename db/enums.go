package db

type QueueEntryState int

const (
	QueueEntryStateQUEUED = iota
	QueueEntryStateRUNNING
	QueueEntryStateCOMPLETED
	QueueEntryStateFAILED
)
