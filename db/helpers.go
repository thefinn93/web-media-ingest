package db

import (
	"context"
	"database/sql"
	"embed"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
	goose "github.com/pressly/goose/v3"

	"gitlab.com/thefinn93/web-media-ingest/config"
)

//go:embed migrations
var migrations embed.FS

func Migrate() error {
	_, dbConn, err := Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	goose.SetBaseFS(migrations)

	if err := goose.SetDialect("sqlite3"); err != nil {
		return err
	}

	if err := goose.Up(dbConn, "migrations"); err != nil {
		return err
	}

	if err := ClearOldInfoJSON(); err != nil {
		return err
	}

	if err := ClearCompletedJobs(); err != nil {
		return err
	}

	return nil
}

// Get the database and closable DB object
// example usage:
//
//	  queries, dbConn, err := db.Get()
//	  if err != nil {
//		   return err
//	  }
//	  defer dbConn.Close()
func Get() (*Queries, *sql.DB, error) {
	db, err := sql.Open("sqlite3", config.C.Database)
	if err != nil {
		return nil, nil, fmt.Errorf("error connecting to database: %v", err)
	}

	return New(db), db, nil
}

func (j *Job) SetStatus(ctx context.Context, queries *Queries, state QueueEntryState) error {
	j.Status = int64(state)
	return queries.SetJobStatus(ctx, SetJobStatusParams{
		Url:    j.Url,
		Status: int64(state),
	})
}

func ClearOldInfoJSON() error {
	queries, dbc, err := Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	err = queries.ClearOldInfoJSON(context.Background())
	if err != nil {
		return err
	}

	return nil
}

func ClearCompletedJobs() error {
	queries, dbc, err := Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	err = queries.ClearCompletedJobs(context.Background())
	if err != nil {
		return err
	}

	return nil
}
