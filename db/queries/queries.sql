-- name: QueueJob :one
INSERT INTO jobs (url, last_change, status, destination) VALUES (?, datetime('now'), 0, ?) RETURNING *;

-- name: GetQueue :many
SELECT * FROM jobs;

-- name: GetNextJob :one
SELECT * FROM jobs WHERE status == 0 ORDER BY last_change ASC LIMIT 1;

-- name: SetJobStatus :exec
UPDATE jobs SET status = ? WHERE url = ?;

-- name: SetInfoJSON :exec
INSERT INTO infojson (url, data, last_used) VALUES (?, ?, datetime('now'));

-- name: GetCachedInfoJSON :one
UPDATE infojson SET last_used = datetime('now') WHERE url = ? RETURNING data;

-- name: ClearOldInfoJSON :exec
DELETE FROM infojson WHERE last_used < datetime('now', '-1 hour');

-- name: ClearCompletedJobs :exec
DELETE FROM jobs WHERE STATUS = 2 AND last_change < datetime('now', '-24 hour');
