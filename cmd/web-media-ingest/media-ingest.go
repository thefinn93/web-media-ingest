package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/labstack/gommon/log"

	"gitlab.com/thefinn93/web-media-ingest/config"
	"gitlab.com/thefinn93/web-media-ingest/db"
	"gitlab.com/thefinn93/web-media-ingest/server"
	"gitlab.com/thefinn93/web-media-ingest/worker"
	"gitlab.com/thefinn93/web-media-ingest/ytdlp"
)

func main() {
	log.SetLevel(log.DEBUG)
	log.SetHeader(`${time_rfc3339} ${short_file}:${line}`)

	log.Info("Starting web-media-ingest ", config.Version)

	if err := config.Load(); err != nil {
		log.Fatal("failed to load config: ", err)
	}

	if err := db.Migrate(); err != nil {
		log.Fatal("database migration failed: ", err)
	}

	if _, err := exec.LookPath("yt-dlp"); errors.Is(err, exec.ErrNotFound) {
		log.Info("yt-dlp not found, installing")
		err := ytdlp.Update()
		if err != nil {
			log.Error("error updating yt-dlp: ", err)
		}
	} else if err != nil {
		log.Error("error checking path for yt-dlp: ", err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	shuttingDown := false

	go backgroundTimers(ctx)

	go func() {
		if err := server.Start(); err != nil {
			log.Error(err)
		}
	}()

	var workersWg sync.WaitGroup
	log.Info("starting ", config.C.WorkerCount, " workers")
	for i := 0; i < config.C.WorkerCount; i++ {
		name := fmt.Sprintf("worker-%d", i)
		workersWg.Add(1)
		go func() {
			defer workersWg.Done()
			for !shuttingDown {
				worker.Work(ctx, name)
			}
		}()
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	for {
		signal := <-signals
		switch signal {
		case syscall.SIGINT, syscall.SIGTERM:
			shuttingDown = true
			cancel()

			log.Info("shutting down http server")
			if err := server.Shutdown(); err != nil {
				log.Error("error shutting down local rpc server: ", err)
			}

			log.Info("waiting for workers to exit")
			workersWg.Wait()

			log.Info("shutdown complete")
			return
		}
	}
}

func backgroundTimers(ctx context.Context) {
	for {
		select {
		case <-time.NewTicker(time.Minute * 15).C:
			err := db.ClearOldInfoJSON()
			if err != nil {
				log.Error("error clearing old info JSON: ", err)
			}

		case <-time.NewTicker(time.Hour).C:
			err := db.ClearCompletedJobs()
			if err != nil {
				log.Error("error clearing old completed jobs: ", err)
			}

		case <-time.NewTicker(time.Hour * 12).C:
			err := ytdlp.Update()
			if err != nil {
				log.Error("error updating yt-dlp: ", err)
			}

		case <-ctx.Done():
			return
		}
	}

}
