package worker

import (
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"unicode"

	"github.com/labstack/gommon/log"
	"gitlab.com/thefinn93/web-media-ingest/config"
	"gitlab.com/thefinn93/web-media-ingest/notify"
	"golang.org/x/net/html"
)

var validExtensions = []string{"mkv", "webm", "mp4", "avi"}

// nfoTVShow implements some of the fields described here: https://kodi.wiki/view/NFO_files/TV_shows#nfo_Tags
type nfoTVShow struct {
	XMLName   xml.Name `xml:"tvshow"`
	Title     string   `xml:"title,omitempty"`
	Plot      string   `xml:"plot,omitempty"`
	MPAA      string   `xml:"mpaa,omitempty"`
	ID        string   `xml:"id,omitempty"`
	Genre     string   `xml:"genre,omitempty"`
	Premiered string   `xml:"premiered,omitempty"`
	Studio    string   `xml:"studio,omitempty"`
}

type nfoEpisodeDetails struct {
	XMLName xml.Name `xml:"episodedetails"`
	Title   string   `xml:"title,omitempty"`
	Episode int      `xml:"episode,omitempty"`
	Season  int      `xml:"season,omitempty"`
	Aired   string   `xml:"aired,omitempty"`
	Plot    string   `xml:"plot,omitempty"`
	Thumb   string   `xml:"thumb,omitempty"`
}

func cleanFilename(s string) string {
	var builder strings.Builder
	for _, char := range s {

		// there has got to be a better way to do this
		switch { // should mostly strip out everything that isn't allowed in a filename. there are better ways of doing this
		case unicode.IsDigit(char):
		case unicode.IsLetter(char):
		case char == ' ':
		case char == '.':
		case char == '-':
		default:
			continue
		}
		builder.WriteRune(char)
	}

	return builder.String()
}

// build and write NFO file(s) from a notify.Info
func postprocess(destination string, downloadedFile string, info notify.Info) error {
	if info.Extractor == "" {
		return fmt.Errorf("invalid infojson: missing extractor or uploader ID: %+v", info)
	}

	if destination == "" {
		destination = config.C.DownloadDestinations[config.DefaultDestination]
	}

	showDirName := fmt.Sprintf("%s-%s", info.Extractor, info.UploaderID)
	if info.UploaderID == "" {
		showDirName = info.Extractor
	}

	showDir := filepath.Join(destination, cleanFilename(showDirName))

	showNfoFilename := filepath.Join(showDir, "tvshow.nfo")
	log.Debugf("ensuring show nfo: %s", showNfoFilename)

	_, err := os.Stat(showNfoFilename)
	if os.IsNotExist(err) { // TODO: detect how old the tvshow.nfo is and periodically refresh
		if err := os.MkdirAll(showDir, 0755); err != nil {
			return err
		}

		if err := writeShowNFO(showDir, info); err != nil {
			return fmt.Errorf("error preparing nfo: %v", err)
		}
	} else if err != nil {
		return err
	}

	seasonDir := filepath.Join(showDir, "Season 01") // it's always season 1
	if err := os.MkdirAll(seasonDir, 0755); err != nil {
		return err
	}

	destBaseFilenameParts := []string{}
	if info.Uploader != "" {
		destBaseFilenameParts = append(destBaseFilenameParts, info.Uploader)
	}

	if info.UploadDate != nil {
		destBaseFilenameParts = append(destBaseFilenameParts, info.UploadDate.Format("2006-01-02"))
	}

	if info.Title != "" {
		destBaseFilenameParts = append(destBaseFilenameParts, info.Title)
	} else {
		destBaseFilenameParts = append(destBaseFilenameParts, info.ID)
	}

	destBaseFilename := strings.Join(destBaseFilenameParts, " - ")

	srcDir := filepath.Dir(downloadedFile)

	files, err := os.ReadDir(srcDir)
	if err != nil {
		return err
	}

	for _, f := range files {
		if f.IsDir() {
			continue
		}

		for _, ext := range validExtensions {
			if !strings.HasSuffix(f.Name(), ext) {
				continue
			}

			src := filepath.Join(srcDir, f.Name())
			dest := filepath.Join(seasonDir, fmt.Sprintf("%s.%s", destBaseFilename, ext))

			log.Debug("copying ", src, " to ", dest)
			err = copy(src, dest)
			if err != nil {
				return err
			}
		}
	}

	nfoFilename := filepath.Join(seasonDir, fmt.Sprintf("%s.nfo", destBaseFilename))
	log.Debug("writing episode nfo to ", nfoFilename)
	f, err := os.Create(nfoFilename)
	if err != nil {
		return err
	}
	defer f.Close()

	aired := ""
	if info.UploadDate != nil {
		aired = info.UploadDate.Format("2006-01-02")
	}

	err = xml.NewEncoder(f).Encode(nfoEpisodeDetails{
		Title:  info.Title,
		Aired:  aired,
		Plot:   info.Description,
		Thumb:  info.GetThumbnailURL(),
		Season: 1,
	})
	if err != nil {
		return err
	}

	return nil
}

func writeShowNFO(showDir string, info notify.Info) error {
	showNfoFilename := filepath.Join(showDir, "tvshow.nfo")
	log.Debug("creating nfo file for show: ", showNfoFilename)

	nfo := nfoTVShow{
		Title: info.Uploader,
		ID:    info.UploaderID,
	}

	if info.ChannelURL == "" {
		return nil
	}

	metatags, err := getHTMLMetatags(info.ChannelURL)
	if err != nil {
		return fmt.Errorf("error getting channel data: %v", err)
	}

	if description, ok := metatags["og:description"]; ok {
		nfo.Plot = description
	}

	if image, ok := metatags["og:image"]; ok {
		err := downloadFile(image, filepath.Join(showDir, "poster.jpg")) // TODO: what if it's not a jpg?
		if err != nil {
			log.Error("error downloading poster: ", err)
		}
	}

	f, err := os.Create(showNfoFilename)
	if err != nil {
		return err
	}
	defer f.Close()

	// TODO: scrape more uploader metadata
	err = xml.NewEncoder(f).Encode(nfo)
	if err != nil {
		return fmt.Errorf("error encoding %s: %v", showNfoFilename, err)
	}

	return nil
}

func getHTMLMetatags(channelURL string) (map[string]string, error) {
	resp, err := http.Get(channelURL)
	if err != nil {
		return map[string]string{}, err
	}
	defer resp.Body.Close()

	metaValues := map[string]string{}

	doc, err := html.Parse(resp.Body)
	if err != nil {
		return map[string]string{}, fmt.Errorf("error parsing html from %s: %v", channelURL, err)
	}

	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "meta" {
			var property string
			var value string
			for _, a := range n.Attr {
				switch a.Key {
				case "property":
					property = a.Val
				case "content":
					value = a.Val
				}
			}
			if property != "" && value != "" {
				metaValues[property] = value
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)

	return metaValues, nil
}

func downloadFile(url, dest string) error {
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("error downloading %s: %v", url, err)
	}
	defer resp.Body.Close()

	f, err := os.Create(dest)
	if err != nil {
		return fmt.Errorf("error opening %s for writing: %v", dest, err)
	}
	defer f.Close()

	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return fmt.Errorf("error downloading %s to %s: %v", url, dest, err)
	}

	return nil
}
