package worker

import (
	"fmt"
	"testing"
)

func TestGetMetaTags(t *testing.T) {
	metatags, err := getHTMLMetatags("https://www.youtube.com/@BeauoftheFifthColumn")
	if err != nil {
		t.Error(err)
	}

	for k, v := range metatags {
		fmt.Println(k, "=", v)
	}

	for k, v := range map[string]string{
		"og:site_name": "YouTube",
		"og:title":     "Beau of the Fifth Column",
	} {
		if metatags[k] != v {
			t.Errorf("unexpected %s metatag value %s, expected %s", k, metatags[k], v)
		}
	}
}
