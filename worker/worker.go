package worker

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/labstack/gommon/log"

	"gitlab.com/thefinn93/web-media-ingest/db"
	"gitlab.com/thefinn93/web-media-ingest/notify"
	"gitlab.com/thefinn93/web-media-ingest/ytdlp"
)

var jobch = make(chan db.Job, 100)

func Work(ctx context.Context, name string) {
	logger := log.New(name)

	queries, dbc, err := db.Get()
	if err != nil {
		logger.Error("error connecting to database: ", err)
		return
	}
	defer dbc.Close()

	for {
		job, err := queries.GetNextJob(ctx)
		if err == sql.ErrNoRows {
			break
		}
		if err != nil {
			log.Error("error getting next job from queue: ", err)
		}

		log.Debug("processing backlog job")
		workAndUpdateJob(ctx, queries, job)
	}

	for {
		select {
		case job := <-jobch:
			workAndUpdateJob(ctx, queries, job)
		case <-ctx.Done():
			return
		}
	}
}

// AddJob adds a job to the database and adds it to the active queue
func AddJob(ctx context.Context, queries *db.Queries, u string, dest string) error {
	job, err := queries.QueueJob(ctx, db.QueueJobParams{Url: u, Destination: dest})
	if err != nil {
		return err
	}

	event := notify.Event{Job: job}

	infoJSON, err := queries.GetCachedInfoJSON(ctx, event.Job.Url)
	if err != nil {
		if err != sql.ErrNoRows {
			return err
		}
	} else {
		err = json.Unmarshal(infoJSON, &event.Info)
		if err != nil {
			return err
		}
	}

	if err := notify.Broadcast(event); err != nil {
		return err
	}

	jobch <- job

	return nil
}

func workAndUpdateJob(ctx context.Context, queries *db.Queries, job db.Job) {
	err := work(ctx, queries, job)
	if err != nil {
		log.Errorf("error working on job %s: %v", job.Url, err)

		job.Status = db.QueueEntryStateFAILED
		err := queries.SetJobStatus(context.Background(), db.SetJobStatusParams{
			Url:    job.Url,
			Status: int64(job.Status),
		})
		if err != nil {
			log.Error("error storing job failed status: ", err)
		}

		job.Status = db.QueueEntryStateFAILED
		event := notify.Event{
			Job:   job,
			Error: err,
		}
		if err := notify.Broadcast(event); err != nil {
			log.Error("error broadcasting update about job state: ", err)
		}
	}
}

func work(ctx context.Context, queries *db.Queries, job db.Job) error {
	log.Debug("beginning download for ", job.Url)

	if err := job.SetStatus(ctx, queries, db.QueueEntryStateRUNNING); err != nil {
		return err
	}

	dir, err := mktemp()
	if err != nil {
		return fmt.Errorf("error creating temporary download directory: %v", err)
	}
	defer func() {
		err := os.RemoveAll(dir)
		if err != nil {
			log.Error("error cleaning up temp dir: ", err)
		}
	}()

	filename, info, err := ytdlp.Download(ctx, queries, job, dir)
	if err != nil {
		return err
	}

	if err := postprocess(job.Destination, filename, info); err != nil {
		return fmt.Errorf("error during post-processing: %v", err)
	}

	job.Status = db.QueueEntryStateCOMPLETED
	err = queries.SetJobStatus(context.Background(), db.SetJobStatusParams{
		Url:    job.Url,
		Status: int64(job.Status),
	})
	if err != nil {
		return err
	}
	if err := notify.Broadcast(notify.Event{Job: job, Info: &info}); err != nil {
		return fmt.Errorf("error updating job state: %v", err)
	}

	log.Info("finished downloading ", job.Url)

	return nil
}

func mktemp() (string, error) {
	dir, err := os.MkdirTemp(os.TempDir(), "web-media-ingest-")
	if err != nil {
		return "", err
	}

	if err := os.Chmod(dir, 0755); err != nil {
		return "", err
	}

	return dir, nil
}

// os.Rename but it works across mount points
func copy(src, dest string) error {
	s, err := os.Open(src)
	if err != nil {
		return err
	}
	defer s.Close()

	d, err := os.Create(dest)
	if err != nil {
		return err
	}

	_, err = io.Copy(d, s)
	if err != nil {
		return err
	}

	return nil
}
