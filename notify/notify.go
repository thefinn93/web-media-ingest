package notify

import (
	"fmt"
	"math/rand"
	"sync"

	"github.com/labstack/gommon/log"

	"gitlab.com/thefinn93/web-media-ingest/db"
)

type JobState string

type EventReceiver func(Event) error

type Event struct {
	Job       db.Job `json:"job,omitempty"`        // Job being updated in this event
	Info      *Info  `json:"info,omitempty"`       // Info describes metadata about the media being retreived
	Error     error  `json:"error,omitempty"`      // Error is non-nill when JobState is JobStateFAILED
	ResultURL string `json:"result_url,omitempty"` // ResultURL is the URL that the resulting file is available at when the job has completed
}

var (
	receivers      = map[string]EventReceiver{}
	receiversMutex sync.Mutex
)

func Register(name string, r EventReceiver) func() {
	receiversMutex.Lock()
	defer receiversMutex.Unlock()

	// find a name that doesn't conflict with any existing receiver
	for {
		key := fmt.Sprintf("%s-%d", name, rand.Uint32())
		_, ok := receivers[key]
		if ok {
			continue
		}

		name = key
		break
	}

	log.Debug("registering receiver ", name)
	receivers[name] = r

	return unregister(name)

}

func unregister(key string) func() {
	return func() {
		log.Debug("unregistering receiver ", key)

		receiversMutex.Lock()
		defer receiversMutex.Unlock()

		delete(receivers, key)
	}
}

func Broadcast(e Event) error {
	log.Debug("broadcasting update to job: ID=", e.Job.ID, " URL=", e.Job.Url, " LastChange=", e.Job.LastChange, " Status=", e.Job.Status)
	for i, r := range receivers {
		log.Debug("notifying receiver ", i, "/", len(receivers))
		err := r(e)
		if err != nil {
			log.Warn("error broadcasting event update: ", err)
		}
	}

	log.Debug("notified ", len(receivers), " receivers")

	return nil
}
