package notify

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

// intstr is an int that can also be parsed from a JSON string via strconv.Parseint
type intstr struct{ int }

// TimeStr is a date/time in one of the yt-dlp output formats
type TimeStr struct{ time.Time }

type Info struct {
	Extractor      string   `json:"extractor,omitempty"`
	UploaderID     string   `json:"uploader_id,omitempty"`
	Uploader       string   `json:"uploader,omitempty"`
	UploaderURL    string   `json:"uploader_url,omitempty"`
	Title          string   `json:"title,omitempty"`
	DurationString string   `json:"duration_string,omitempty"`
	Width          float64  `json:"width,omitempty"`
	Height         float64  `json:"height,omitempty"`
	ACodec         string   `json:"acodec,omitempty"`
	VCodec         string   `json:"vcodec,omitempty"`
	Channel        string   `json:"channel,omitempty"`
	ChannelURL     string   `json:"channel_url,omitempty"`
	FPS            float64  `json:"fps,omitempty"`
	Ext            string   `json:"ext,omitempty"`
	ID             string   `json:"id,omitempty"`
	UploadDate     *TimeStr `json:"upload_date,omitempty"`
	Description    string   `json:"description,omitempty"`

	Thumbnails []thumbnail `json:"thumbnails,omitempty"`
}

type thumbnail struct {
	URL        string `json:"url"`
	Preference int    `json:"preference"`
	ID         string `json:"id"`
	Height     intstr `json:"height"`
	Width      int    `json:"width"`
	Resolution string `json:"resolution"`
}

func (s *intstr) UnmarshalJSON(data []byte) error {
	var value int64
	// if it starts with a double quote, it's a string
	if bytes.HasPrefix(data, []byte("\"")) {
		var strval string
		err := json.Unmarshal(data, &strval)
		if err != nil {
			return err
		}

		value, err = strconv.ParseInt(strval, 10, 64)
		if err != nil {
			return err
		}
	} else {
		if err := json.Unmarshal(data, &value); err != nil {
			return err
		}
	}

	s.int = int(value)

	return nil
}

func (i Info) GetThumbnailURL() string {
	if len(i.Thumbnails) == 0 {
		return ""
	}

	u := ""
	var best int
	for _, thumbnail := range i.Thumbnails {
		if thumbnail.Preference <= best && u != "" {
			continue
		}

		// skip thumbnails that have no resultion string value
		// todo: should this be limited to YouTube?
		if thumbnail.Resolution == "" {
			continue
		}

		best = thumbnail.Preference
		u = thumbnail.URL
	}

	return u
}

func (d *TimeStr) UnmarshalJSON(data []byte) error {
	var strval string
	if err := json.Unmarshal(data, &strval); err != nil {
		return fmt.Errorf("error unmarshaling %s: %v", string(data), err)
	}

	timeval, err := time.Parse("20060102", strval)
	if err != nil {
		return err
	}

	d.Time = timeval

	return nil
}

func (d TimeStr) Format(layout string) string {
	return d.Time.Format(layout)
}
