package notify

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/labstack/gommon/log"

	"gitlab.com/thefinn93/web-media-ingest/config"
)

func notifyJellyfin(event Event) error {
	if config.C.Notifiers.Jellyfin.URL == "" {
		return nil
	}

	u := config.C.Notifiers.Jellyfin.URL + "/Library/Media/Updated"

	body := map[string][]map[string]string{
		"Updates": {
			{
				"Path": event.Job.Destination,
				"Type": "Modified",
			},
		},
	}

	marshaledBody, err := json.Marshal(body)
	if err != nil {
		return fmt.Errorf("error marshaling POST body: %v", err)
	}

	req, err := http.NewRequest("POST", u, bytes.NewBuffer(marshaledBody))
	if err != nil {
		return err
	}

	req.Header.Set("Authorization", fmt.Sprintf("MediaBrowser Token=\"%s\"", config.C.Notifiers.Jellyfin.APIKey))
	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusNoContent {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Errorf("error reading response body from jellyfin: %v", err)
			body = []byte{}
		}

		return fmt.Errorf("error requesting %s: got status %d expected 204. Body: %s", u, resp.StatusCode, string(body))
	}

	return nil
}

func init() {
	Register("jellyfin", notifyJellyfin)
}
