package notify_test

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"
	"time"

	"gitlab.com/thefinn93/web-media-ingest/notify"
)

func TestTimeStrUnmarshalJSON(t *testing.T) {
	type parsable struct{ T notify.TimeStr }
	marshaledTemplate := `{"T": "%s"}`

	now, _ := time.Parse(time.RFC3339, "2023-03-12T00:00:00Z")

	for _, format := range []string{"20060102"} {
		marshaled := fmt.Sprintf(marshaledTemplate, now.Format(format))
		var unmarshaled parsable

		err := json.NewDecoder(strings.NewReader(marshaled)).Decode(&unmarshaled)
		if err != nil {
			t.Errorf("error parsing '%s': %s", marshaled, err)
			continue
		}

		if !now.Equal(unmarshaled.T.Time) {
			t.Errorf("expected %s, got %s: %s", now.Format(time.RFC3339), unmarshaled.T.Format(time.RFC3339), marshaled)
		}
	}

}
