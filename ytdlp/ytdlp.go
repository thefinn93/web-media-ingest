package ytdlp

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/labstack/gommon/log"

	"gitlab.com/thefinn93/web-media-ingest/config"
	"gitlab.com/thefinn93/web-media-ingest/db"
	"gitlab.com/thefinn93/web-media-ingest/notify"
)

const infoJSONFile = "media.info.json"

// GetInfo uses yt-dlp to scan a URL and returns a the result as an Info object,
// using the database to cache results so subsequent requests for the same URL
// do not require re-scraping scraping
func GetInfo(ctx context.Context, queries *db.Queries, u string) (notify.Info, error) {
	dir, err := os.MkdirTemp("", "web-media-ingest")
	if err != nil {
		return notify.Info{}, err
	}
	defer os.RemoveAll(dir)

	infoFilename, err := WriteInfoToDir(ctx, queries, u, dir)
	if err != nil {
		return notify.Info{}, err
	}

	f, err := os.Open(infoFilename)
	if err != nil {
		return notify.Info{}, err
	}
	defer f.Close()

	var info notify.Info
	if err = json.NewDecoder(f).Decode(&info); err != nil {
		return notify.Info{}, err
	}

	return info, nil
}

// WriteInfoToDir uses yt-dlp to scrape the infojson for a given URL. It will uses the database to cache the results
func WriteInfoToDir(ctx context.Context, queries *db.Queries, u string, dir string) (string, error) {
	filename := filepath.Join(dir, infoJSONFile)

	infojson, err := queries.GetCachedInfoJSON(ctx, u)
	if err == sql.ErrNoRows {
		log.Debug("no cached infojson for ", u, ", invoking yt-dlp to get it")
		err := ytdlp(u, dir, "--write-info-json", "--skip-download")
		if err != nil {
			return "", err
		}

		infojson, err = os.ReadFile(filename)
		if err != nil {
			return "", err
		}
		err = queries.SetInfoJSON(ctx, db.SetInfoJSONParams{
			Url:  u,
			Data: infojson,
		})
		if err != nil {
			return "", err
		}
	} else if err != nil {
		return "", fmt.Errorf("error checking for cached metadata: %v", err)
	} else {
		log.Debug("found cached infojson in database, writing to disk")

		err = os.WriteFile(filename, infojson, 0644)
		if err != nil {
			return "", err
		}
	}

	return filename, nil
}

func Download(ctx context.Context, queries *db.Queries, job db.Job, dir string) (string, notify.Info, error) {
	infojson, err := WriteInfoToDir(ctx, queries, job.Url, dir)
	if err != nil {
		return "", notify.Info{}, err
	}

	f, err := os.Open(infojson)
	if err != nil {
		return "", notify.Info{}, err
	}
	defer f.Close()

	var info notify.Info
	err = json.NewDecoder(f).Decode(&info)
	if err != nil {
		return "", notify.Info{}, err
	}

	if err := notify.Broadcast(notify.Event{Job: job, Info: &info}); err != nil {
		log.Error("error broadcasting job running state: ", err)
	}

	err = ytdlp(job.Url, dir, "--remux-video", "mkv", "--embed-metadata", "--write-subs", "--sponsorblock-mark", "all", "--load-info-json", infojson)
	if err != nil {
		return "", notify.Info{}, err
	}

	return filepath.Join(dir, fmt.Sprintf("media.%s", info.Ext)), info, nil
}

func ytdlp(u string, dir string, extraOpts ...string) error {
	args, err := options(u, extraOpts...)
	if err != nil {
		return err
	}
	youtubedl := exec.Command("yt-dlp", args...)
	youtubedl.Dir = dir
	log.Debug("running command: ", youtubedl.Args)
	start := time.Now()
	out, err := youtubedl.Output()
	log.Debug("yt-dlp time ", time.Since(start))
	if err != nil {
		msg := string(out)
		if exitError, ok := err.(*exec.ExitError); ok {
			log.Warn("yt-dlp stdout:", string(out))
			log.Warn("yt-dlp stderr:", string(exitError.Stderr))
			version := Version()
			log.Warn("yt-dlp version:", version)
			msg = string(exitError.Stderr) + "\n\nyt-dlp version: " + version
		}
		return fmt.Errorf("error running yt-dlp:\n%s\n\n%v", msg, err)
	}

	return nil
}

func options(u string, extra ...string) (args []string, err error) {
	parsedURL, err := url.Parse(u)
	if err != nil {
		return []string{}, fmt.Errorf("error parsing url %s: %v", u, err)
	}

	switch parsedURL.Host {
	case "www.tiktok.com", "tiktok.com":
		args = append(args, "--add-header", "User-Agent: Mozilla/5.0")
	}

	args = append(args, extra...)
	args = append(args, config.C.ExtraYtdlpOptions...)
	args = append(args, "-o", "media.%(ext)s", u)

	return args, nil
}

func Version() string {
	version, err := exec.Command("yt-dlp", "--version").Output()
	if err != nil {
		log.Warn("error checking yt-dlp version: ", err)
		return ""
	}
	return strings.TrimSpace(string(version))
}

// Update ytdlp from git
func Update() error {
	oldversion := Version()

	log.Info("updating yt-dlp. current version: ", oldversion)

	cmd := exec.Command("pip", "install", "-U", "git+https://github.com/yt-dlp/yt-dlp.git")
	cmd.Stderr = os.Stderr
	_, err := cmd.Output()
	if err != nil {
		return fmt.Errorf("error running %s %s: %v", cmd.Path, cmd.Args, err)
	}

	newversion := Version()
	if oldversion != newversion {
		log.Info("updated successfully, now running yt-dlp ", newversion)
	} else {
		log.Debug("no update available")
	}

	return nil
}
