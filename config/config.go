package config

import (
	"encoding/json"
	"os"

	"github.com/labstack/gommon/log"
)

const DefaultDestination = "default"

var Version = "unknown version"

var ConfigFiles = []string{
	"/etc/web-media-ingest.json",
	"web-media-ingest.json",
}

type Config struct {
	Database             string            `json:"database"`              // Database is the path used for the sqlite database
	ExtraYtdlpOptions    []string          `json:"extra_ytdlp_options"`   // ExtraYtdlpOptions are passed to every yt-dlp invocation
	WorkerCount          int               `json:"worker_count"`          // How many workers to run
	Bind                 string            `json:"bind"`                  // which interface and port to bind to, go format
	Notifiers            Notifiers         `json:"notifiers"`             // external services to notify of change
	DownloadDestinations map[string]string `json:"download_destinations"` // map[name]path of download destinations. API clients specify name and they are downloaded to the specified path. Make at least one named "default"
	SkipCleanup          bool              `json:"skip_cleanup"`          // set to true to not delete temp files
}

type Notifiers struct {
	Jellyfin NotifierJellyfin `json:"jellyfin"`
}

type NotifierJellyfin struct {
	URL    string `json:"url"`
	APIKey string `json:"api_key"`
}

var C = Config{
	Database:             "web-media-ingest.db",
	WorkerCount:          2,
	Bind:                 ":8080",
	DownloadDestinations: map[string]string{DefaultDestination: "output"},
}

func Load() error {
	loaded := false
	for _, filename := range ConfigFiles {
		f, err := os.Open(filename)
		if err != nil {
			if os.IsNotExist(err) {
				continue
			}

			log.Warn("error loading config from ", filename, ": ", err)
			continue
		}
		defer f.Close()

		err = json.NewDecoder(f).Decode(&C)
		if err != nil {
			log.Warn("error loading config from ", filename, ": ", err)
			continue
		}

		loaded = true
	}

	if !loaded {
		log.Warn("no config files loaded. Checked files: ", ConfigFiles)
	}

	return nil
}
