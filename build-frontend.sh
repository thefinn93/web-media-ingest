#!/bin/bash
set -exuo pipefail

# prepare the build image. Does as a separate step to take advantage of caching
podman build -t web-media-ingest-build -f - . <<EOF
FROM debian:bookworm
RUN apt-get update && apt-get install -y yarnpkg golang-go && apt-get clean
EOF

podman run --rm -v $(pwd):/build/web-media-ingest -w /build/web-media-ingest -e GOPROXY="${GOPROXY:-direct}" -i web-media-ingest-build bash -x <<EOF
yarnpkg install
node_modules/.bin/vite build --emptyOutDir
EOF
