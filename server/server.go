package server

import (
	"context"
	"embed"
	"io/fs"
	"net/http"

	echo "github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/thefinn93/web-media-ingest/config"
)

var (
	//go:embed static
	static embed.FS

	e *echo.Echo
)

func Start() error {
	e = echo.New()
	e.HideBanner = true

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: `${time_rfc3339} ${remote_ip} ${method} ${uri} ${user_agent} ${status} ${latency_human}` + "\n",
	}))
	e.Use(apiError)
	e.Use(middleware.Recover())

	assets, _ := fs.Sub(static, "static/assets")
	e.StaticFS("/assets", assets)
	e.FileFS("/", "static/index.html", static)

	e.GET("/api/events", events)

	e.PUT("/api/queue", queueAdd)
	e.GET("/api/queue", queueGet)

	e.POST("/api/inspect", inspect)

	return e.Start(config.C.Bind)
}

func Shutdown() error {
	for ch := range eventClients {
		close(ch)
	}

	return e.Shutdown(context.Background())
}

func apiError(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		err := next(c)
		if err != nil {
			c.Logger().Error(err)
			return c.JSON(http.StatusInternalServerError, map[string]string{
				"error": err.Error(),
			})
		}
		return nil
	}
}
