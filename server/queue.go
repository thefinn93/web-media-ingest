package server

import (
	"database/sql"
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"
	"gitlab.com/thefinn93/web-media-ingest/config"
	"gitlab.com/thefinn93/web-media-ingest/db"
	worker "gitlab.com/thefinn93/web-media-ingest/worker"
)

type AddToQueueRequest struct {
	URL         string `json:"url"`
	Destination string `json:"destination"`
}

func queueAdd(c echo.Context) error {
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	var body AddToQueueRequest

	if err := c.Bind(&body); err != nil {
		return fmt.Errorf("invalid request body: %v", err)
	}

	destination, ok := config.C.DownloadDestinations[body.Destination]
	if !ok {
		if body.Destination == "" {
			destination = config.C.DownloadDestinations[config.DefaultDestination]
		} else {
			return fmt.Errorf("unknown destination %s", body.Destination)
		}
	}

	err = worker.AddJob(c.Request().Context(), queries, body.URL, destination)
	if err != nil {
		return fmt.Errorf("error adding to queue: %v", err)
	}

	return c.NoContent(http.StatusCreated)
}

func queueGet(c echo.Context) error {
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	queue, err := queries.GetQueue(c.Request().Context())
	if err != nil {
		if err == sql.ErrNoRows {
			return c.JSON(http.StatusOK, queue)
		}
		return fmt.Errorf("error getting queue from database: %v", err)
	}
	return c.JSON(http.StatusOK, queue)
}
