package server

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/thefinn93/web-media-ingest/db"
	"gitlab.com/thefinn93/web-media-ingest/ytdlp"
)

type ScanURLRequest struct {
	URL string `json:"url"`
}

func inspect(c echo.Context) error {
	var body ScanURLRequest
	if err := c.Bind(&body); err != nil {
		return err
	}

	if body.URL == "" {
		return errors.New("must specify a URL in request body")
	}

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	c.Logger().Debug("getting info for ", body.URL)

	info, err := ytdlp.GetInfo(c.Request().Context(), queries, body.URL)
	if err != nil {
		return fmt.Errorf("error inspecting URL: %v", err)
	}

	return c.JSON(http.StatusOK, info)
}
