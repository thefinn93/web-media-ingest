package server

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"

	echo "github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"

	"gitlab.com/thefinn93/web-media-ingest/config"
	"gitlab.com/thefinn93/web-media-ingest/db"
	"gitlab.com/thefinn93/web-media-ingest/notify"
)

type eventWrapper struct {
	ID    string
	Event string
	Data  interface{}
}

var (
	eventClients     = map[chan eventWrapper]bool{}
	eventCLientsLock sync.Mutex
	headers          = map[string]string{
		"Content-Type":                "text/event-stream",
		"Cache-Control":               "no-cache",
		"Connection":                  "keep-alive",
		"Access-Control-Allow-Origin": "*",
	}
)

func events(c echo.Context) error {
	w := c.Response().Writer
	flusher := w.(http.Flusher)

	h := w.Header()
	for k, v := range headers {
		h.Set(k, v)
	}

	w.WriteHeader(http.StatusOK)
	flusher.Flush()

	ch := make(chan eventWrapper, 100)
	ch <- eventWrapper{Event: "version", Data: config.Version}
	registerEventClient(ch)
	defer unregisterEventClient(ch)

	ctx := c.Request().Context()

	go func() {
		queries, dbConn, err := db.Get()
		if err != nil {
			log.Error("error connecting to db: ", err)
			return
		}
		defer dbConn.Close()

		jobs, err := queries.GetQueue(ctx)
		if err != nil && err != sql.ErrNoRows {
			log.Error("error getting queue from db: ", err)
			return
		}

		for _, job := range jobs {
			var info *notify.Info

			infoJSON, err := queries.GetCachedInfoJSON(ctx, job.Url)
			if err != nil {
				if err != sql.ErrNoRows {
					log.Error("unexpected error checking cache for infojson about ", job.Url, ": ", err)
				}
			} else {
				if err := json.Unmarshal(infoJSON, &info); err != nil {
					log.Error("unexpected error unmarshaling cached infojson about ", job.Url, ": ", err)
				}
			}

			event := notify.Event{
				Job:  job,
				Info: info,
			}

			ch <- eventWrapper{
				Event: "queue",
				Data:  event,
			}
		}

	}()

	// register notify callback
	unregister := notify.Register("eventsource-client", func(e notify.Event) error {
		ch <- eventWrapper{
			Event: "queue",
			Data:  e,
		}
		return nil
	})
	defer unregister()

	pingTicker := time.NewTicker(time.Second * 10)
	for {
		select {
		case event, open := <-ch:
			if !open {
				return nil
			}

			c.Logger().Debug(event)
			if event.ID != "" {
				fmt.Fprintf(w, "id: %s\n", event.ID)
			}

			if event.Event != "" {
				fmt.Fprintf(w, "event: %s\n", event.Event)
			}

			fmt.Fprint(w, "data: ")

			if err := json.NewEncoder(w).Encode(event.Data); err != nil {
				return err
			}

			fmt.Fprint(w, "\n")
			flusher.Flush()

		case <-pingTicker.C:
			fmt.Fprintf(w, "event: ping\ndata:\n\n")
			flusher.Flush()

		case <-ctx.Done():
			return nil
		}
	}
}

func registerEventClient(ch chan eventWrapper) {
	eventCLientsLock.Lock()
	defer eventCLientsLock.Unlock()

	eventClients[ch] = true
}

func unregisterEventClient(ch chan eventWrapper) {
	eventCLientsLock.Lock()
	defer eventCLientsLock.Unlock()

	delete(eventClients, ch)
	close(ch)
}
